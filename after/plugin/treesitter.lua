local configs = require("nvim-treesitter.configs")

configs.setup({
    ensure_installed = {
        "lua",
        "python",
        "vim",
        "vimdoc",
    },
    auto_install = false,
    ignore_install = { "javascript" },
    sync_install = false,
    highlight = { enable = true },
    indent = { enable = true },
    modules = {},

})
