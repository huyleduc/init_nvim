vim.g.mapleader = " "
vim.keymap.set('n', "<leader>pv", vim.cmd.Ex)
vim.keymap.set('n', '<leader>u', vim.cmd.UndotreeToggle)

vim.keymap.set('n', 'j', 'jzz', { noremap = true })
vim.keymap.set('n', 'k', 'kzz', { noremap = true })

vim.keymap.set('n', 'o', 'o<Esc>', { noremap = true })
vim.keymap.set('n', 'O', 'O<Esc>', { noremap = true })

vim.keymap.set('n', '<C-m>', ':lua vim.lsp.buf.format()<CR>', { noremap = true, silent = true })

