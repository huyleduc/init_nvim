setl shiftwidth=2 tabstop=2


nnoremap <C-r> :terminal python % <CR>
nnoremap <C-b> :buffer # <CR>
